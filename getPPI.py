from Bio.PDB import PDBList, PDBIO, PDBParser
import MDAnalysis as mda
from MDAnalysis.analysis import distances
import numpy as np

import argparse
import os, sys

from io import StringIO

import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score

def getContactMap(complex, sel1, sel2):
    chainA = complex.select_atoms(sel1)
    chainB = complex.select_atoms(sel2)

    cm = np.zeros((chainA.n_residues, chainB.n_residues))
    contacts=[]
    for i,r1 in enumerate(chainA.residues):
        for j,r2 in enumerate(chainB.residues):
            dist_arr = distances.distance_array(r1.atoms.positions, 
                                                r2.atoms.positions)
            cm[i,j] = np.min(dist_arr)
    return cm

def mapResidues(complex, sel):
    chain = complex.select_atoms(sel)

    residueMap = {}

    for i, r in enumerate(chain.residues):
        residueMap[i] = r.resid
    return residueMap

# Get list of close contacts
def getCloseContacts(contactMap, cutoff):
    nx, ny = contactMap.shape

    return np.array(np.where(contactMap < cutoff)).T

def findMedoids(X, n_clusters, cluster_labels, cluster_centers):
    cluster_medoids = np.zeros(cluster_centers.shape)
    for i in range(n_clusters):
        Xc = X[cluster_labels == i]
        dist_2 = np.sum((Xc - cluster_centers[i])**2, axis=1)
        cluster_medoids[i] = Xc[np.argmin(dist_2)]
    return cluster_medoids

def findOptimalNumberOfClusters(closeContacts, dirName):
    # Here we use the silhouette_score to find the optimal number of clusters

    scores = []
    for n_clusters in range(2,11):
        cluster_labels, cluster_centers = clusterCM(closeContacts, n_clusters)
        np.savetxt(dirName + "/centroids_for_Ncl_{}.dat".format(n_clusters),
        findMedoids(closeContacts, n_clusters, cluster_labels, cluster_centers), fmt = "%d")

        silhouette_avg = silhouette_score(closeContacts, cluster_labels)
        print(
            "For n_clusters =",
            n_clusters,
            "The average silhouette_score is :",
            silhouette_avg,
        )

        scores.append([n_clusters, silhouette_avg, np.bincount(cluster_labels)])
        
        plotClusters(closeContacts, n_clusters, cluster_labels, cluster_centers, 
                     silhouette_avg, dirName)

    # finding optimal number of clusters
    scores.sort(key = lambda x: -x[1])
    # if clusters are poor defined, we return one cluster with the medoid contact
    if scores[0][1] < 0.4:
        return (1, findMedoids(closeContacts, 
                               1, 
                               np.zeros(closeContacts.shape[0], dtype=int), 
                               closeContacts.mean(axis=0)))
    maxScore = scores[0][1]
    # Check if the sizes of the clusters are extremely different
    if (max(scores[0][2] + 0.) / min(scores[0][2])) < 20:
        return(scores[0][0], 
               np.loadtxt(dirName + "/centroids_for_Ncl_{}.dat".format(scores[0][0]),
                          dtype=int))

    print("The optimal clustering leads to extremely skewed distributions.")
    print("Checking other clustering with high scores")
    currentCluster = 1
    while (scores[currentCluster][1] > 0.9*maxScore):
        if (max(scores[currentCluster][2] + 0.) / min(scores[currentCluster][2])) > 20:
            currentCluster += 1
            continue
        return(scores[currentCluster][0], 
               np.loadtxt(dirName + "/centroids_for_Ncl_{}.dat".format(scores[currentCluster][0]),
                          dtype=int))
    print("No better clusters found")
    return(scores[0][0], 
           np.loadtxt(dirName + "/centroids_for_Ncl_{}.dat".format(scores[0][0]),
                          dtype=int))


def clusterCM(X, n_clusters):
    clusterer = KMeans(n_clusters=n_clusters)
    return (clusterer.fit_predict(X), clusterer.cluster_centers_)

def plotClusters(X, n_clusters, cluster_labels, cluster_centers, silhouette_avg, dirName):
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.set_size_inches(18, 7)

    sample_silhouette_values = silhouette_samples(X, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        color = cm.nipy_spectral(float(i) / n_clusters)
        ax1.fill_betweenx(
            np.arange(y_lower, y_upper),
            0,
            ith_cluster_silhouette_values,
            facecolor=color,
            edgecolor=color,
            alpha=0.7,
        )

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    colors = cm.nipy_spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(
        X[:, 0], X[:, 1], marker=".", s=30, lw=0, alpha=0.7, c=colors, edgecolor="k"
    )

    # Labeling the clusters
    centers = cluster_centers
    # Draw white circles at cluster centers
    ax2.scatter(
        centers[:, 0],
        centers[:, 1],
        marker="o",
        c="white",
        alpha=1,
        s=200,
        edgecolor="k",
    )

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker="$%d$" % i, alpha=1, s=50, edgecolor="k")

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")

    plt.suptitle(
        "Silhouette analysis for KMeans clustering on sample data with n_clusters = %d"
        % n_clusters,
        fontsize=14,
        fontweight="bold",
    )

    plt.savefig(dirName + "/clustering_with_Ncl_{}".format(n_clusters))

def improveNumberingOfResiduesForClusters(dirName, residueMap1, residueMap2):
    for n_clusters in range(2,11):
        resContacts = np.loadtxt(dirName + "/centroids_for_Ncl_{}.dat".format(n_clusters),
                          dtype=int)
        resContactsCorrected = np.array(list(np.array([residueMap1[x[0]], residueMap2[x[1]]]) 
            for x in resContacts))
        np.savetxt(dirName + 
                   "/centroids_for_Ncl_{}_residue_numbering.dat".format(n_clusters),
                   resContactsCorrected, fmt = "%d")

if __name__ == "__main__":
    parser = argparse.ArgumentParser( \
            description='getPPI is a python script, that extracts the contact \
            information from a protein PDB structure file. It first calculates \
            the cantact map between two chains. Then it cuts those pairs of \
            residues for which the distance is higher than the cutoff (8 A). The \
            remaining pairs are then clustered, and only one pair per cluster \
            remains in the residue pair list that forms the interface. The \
            distance is considered to be the shortest distance between amino acids.')

    parser.add_argument('-pdb', '--pdb',required=True, type = str, \
                help = 'The input PDB ID or local pdb file. The code is checking \
                for "." symbol in the input string. If there is no ".", it forms \
                a request for rcsb.org and downloads a file from there.')

    parser.add_argument('-s1', '--sel1', required=False, type=str, \
                default = 'protein and segid A', \
                help = 'Selection string for chain A. The selection string should be \
                in MDAnalysis format')

    parser.add_argument('-s2', '--sel2', required=False, type=str, \
                default = 'protein and segid B', \
                help = 'Selection string for chain B. The selection string should be \
                in MDAnalysis format')

    args = parser.parse_args(args=None if sys.argv[1:] else ['--help'])

    pdb = args.pdb
    if ".pdb" in pdb:
        filename = pdb
    else:
        # redirecting output to catch the possible error
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        
        pdbl = PDBList()
        filename = pdbl.retrieve_pdb_file(pdb,file_format="pdb",pdir=".")

        # redirecting output back to normal
        sys.stdout = old_stdout
        if "Desired structure doesn't exist" in mystdout.getvalue():
            raise ValueError("getPPI expects either path to PDB file or relevant PDB code")

    # No checks are needed here
    # MDAnalysis has its own error handling
    sel1 = args.sel1
    sel2 = args.sel2

    # We are using default cutoff of 8 angstroms
    cutoff = 8

    proteinComplex = mda.Universe(filename)

    # map between residue indices and residue sequential number
    residueMap1 = mapResidues(proteinComplex, sel1)
    residueMap2 = mapResidues(proteinComplex, sel2)

    # contact map
    print("Calculating contact map")
    contactMap = getContactMap(proteinComplex, sel1, sel2)
    np.savetxt("contact_map.dat", contactMap)
    np.savetxt("contact_map_only_contacts.dat", contactMap<8)

    closeContacts = getCloseContacts(contactMap, cutoff)

    plt.figure(1)
    sns.heatmap(contactMap)
    plt.savefig("contact_map.png")

    plt.figure(2)
    sns.heatmap(contactMap < cutoff)
    plt.savefig("contact_map_only_contacts.png")

    print("Clustering contact map")
    dirCl = "clustering"
    os.makedirs(dirCl, exist_ok=True)

    optimal_n_clusters, resNumberContacts = findOptimalNumberOfClusters(closeContacts, dirCl)

    resResidContacts = np.array(list(np.array([residueMap1[x[0]], residueMap2[x[1]]]) 
                                for x in resNumberContacts))

    np.savetxt("residue_contacts.dat", resResidContacts, fmt = "%d")

    improveNumberingOfResiduesForClusters(dirCl, residueMap1, residueMap2)

    

