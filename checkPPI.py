import MDAnalysis as mda
from MDAnalysis.analysis import distances
import numpy as np

import argparse
import os, sys

import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import warnings

def getRRdist(traj, sel1, sel2, com):
    if not com:
        return np.min(
                distances.distance_array(
                    traj.select_atoms(sel1).atoms.positions,
                    traj.select_atoms(sel2).atoms.positions))
    else:
        return np.linalg.norm(
                traj.select_atoms(sel1).center_of_mass() - 
                traj.select_atoms(sel2).center_of_mass())

def getAllDist(traj, sel1, sel2, resContacts, com):
    dists = np.zeros(len(resContacts))
    for i, contact in enumerate(resContacts):
        dists[i] = getRRdist(traj, 
                             sel1 + " and resid {} and not name H*".format(contact[0]),
                             sel2 + " and resid {} and not name H*".format(contact[1]), com)
    return dists

def getDistForTraj(traj, sel1, sel2, resContacts, com, skip):
    dists = np.zeros((len(traj.trajectory[::skip]), len(resContacts)+1))
    for i, ts in enumerate(traj.trajectory[::skip]):
        dists[i,0] = traj.trajectory.time
        dists[i,1:] = getAllDist(traj, sel1, sel2, resContacts, com)
    return dists

def saveSingleTraj(folder, dists, header, resContacts):
    np.savetxt(folder+"/contact_distances.dat", dists, 
        header = header)

    fig = plt.figure()
    for d in dists.T[1:]:
        plt.plot(dists.T[0], d)
    fig.suptitle("Contact trajectories")
    plt.xlabel("Time (ps)")
    plt.ylabel("Distance (Ang)")
    plt.legend(list("resids_{}_{}".format(x[0],x[1]) for x in resContacts))
    fig.savefig(folder + "/contact_distances.png")

    np.savetxt(folder + "/contact_correlations.dat", np.corrcoef(dists[:,1:].T))
    fig = plt.figure()
    sns.heatmap(np.corrcoef(dists[:,1:].T))
    fig.savefig(folder + "/contact_correlations.png")

def cmpTrajs(folder, dists, dists2, resContacts):
    correlations = []
    for d1, d2, c in zip(dists.T[1:], dists2.T[1:], resContacts):
        l = min(d1.shape[0], d2.shape[0])
        corrcoef = np.correlate(d1[:l], d2[:l]) / \
            np.sqrt(np.correlate(d1[:l], d1[:l]) * np.correlate(d2[:l], d2[:l]))
        fig = plt.figure()
        plt.plot(dists.T[0], d1)
        plt.plot(dists2.T[0], d2)
        fig.suptitle("Comparison of contact resid {} and resid {}. Correlation is {}".\
            format(c[0], c[1], corrcoef))
        plt.xlabel("Time (ps)")
        plt.ylabel("Distance (Ang)")
        plt.legend(["trajectory 1", "trajectory 2"])
        fig.savefig(folder + "/cmp_contact_{}_{}.png".format(c[0], c[1]))
        correlations.append(corrcoef)
    np.savetxt(folder + "/cmp_correlations.dat", np.array(correlations))


if __name__ == "__main__":
    parser = argparse.ArgumentParser( \
            description='checkPPI is a python script, that analyzes the contact \
            information from a protein trajectory file. It first reads \
            the contact map from residue_contacts.dat file. If there is no such \
            file the code reises an error. This file can be obtained with getPPI.py \
            script, or prepared manually. For each pair of residues the distances \
            for each frame are calculated. The distance between two residues is the \
            shortest distance between all heavy atoms of those residues (the distance \
            between center of masses can be also calculated if -com is used). For \
            individual trajectory the plots for each contact distance is plotted and \
            the correlation matrix is computed. When two trajectories are provided, \
            the comparison of contact distances for two trajectories is plotted, \
            alongside with correlation between corresponding contact trajectories.')

    parser.add_argument('-r', '--struct',required=True, type = str, \
                help = 'The input .pdb/.gro/.tpr file.')

    parser.add_argument('-t', '--traj',required=True, type = str, \
                help = 'The input trajectory .xtc/.trr file.')

    parser.add_argument('-r2', '--struct2',required=False, type = str, default=None, \
                help = 'The input .pdb/.gro/.tpr file for the second trajectory.')

    parser.add_argument('-t2', '--traj2',required=False, type = str, default=None, \
                help = 'The input trajectory .xtc/.trr file for the second trajectory.')

    parser.add_argument('-s1', '--sel1', required=False, type=str, \
                default = 'protein and segid A', \
                help = 'Selection string for chain A. The selection string should be \
                in MDAnalysis format')

    parser.add_argument('-s2', '--sel2', required=False, type=str, \
                default = 'protein and segid B', \
                help = 'Selection string for chain B. The selection string should be \
                in MDAnalysis format')

    parser.add_argument('-s12', '--sel12', required=False, type=str, \
                default = None, \
                help = 'Selection string for chain A in the second trajectory. The \
                selection string should be in MDAnalysis format. If not proveded equal to sel1')

    parser.add_argument('-s22', '--sel22', required=False, type=str, \
                default = None, \
                help = 'Selection string for chain B in the second trajectory. The \
                selection string should be in MDAnalysis format. If not proveded equal to sel2')

    parser.add_argument('-sk', '--skip', required=False, type=int, \
                default = 10, \
                help = 'Use every N-th frame for the analysis. Default is 10')

    parser.add_argument('-com', '--com', required=False, type=bool, \
                default = False, \
                help = 'If True, the distance between residues is computed as distance \
                        between COMs of residues')

    args = parser.parse_args(args=None if sys.argv[1:] else ['--help'])

    traj = mda.Universe(args.struct, args.traj)

    secondTraj = False

    if args.struct2 and not args.traj2:
        warnings.warn("The structure file is provided for the second trajectory, \n\
            but the trajectory file is not. The second trajectory is \n\
            not analyzed.", UserWarning)
    if args.traj2 and not args.struct2:
        warnings.warn("The trajectory file is provided for the second trajectory, \n\
            but the structure file is not. The second trajectory is \n\
            not analyzed.", UserWarning)
    if args.struct2 and args.traj2:
        secondTraj = True
        traj2 = mda.Universe(args.struct2, args.traj2)

    try:
        resContacts = np.loadtxt("residue_contacts.dat",dtype = int)
    except InputError:
        print("checkPPI requires the input residue_contacts.dat file which was not found")  

    # No checks are needed here
    # MDAnalysis has its own error handling
    sel1 = args.sel1
    sel2 = args.sel2

    if secondTraj and not args.sel12:
        sel12 = sel1
    else:
        sel12 = args.sel12
    if secondTraj and not args.sel22:
        sel22 = sel2
    else:
        sel22 = args.sel22

    com = args.com

    skip = args.skip

    header = "Time "
    for r1, r2 in resContacts:
        header += "resid_{}_resid_{} ".format(r1, r2)

    dirTraj1 = "main_trajectory"
    print("Getting contact trajectories")
    dists = getDistForTraj(traj, sel1, sel2, resContacts, com, skip)
    os.makedirs(dirTraj1, exist_ok=True)

    saveSingleTraj(dirTraj1, dists, header, resContacts)

    if secondTraj:
        dirTraj2 = "second_trajectory"
        dirCmp   = "trajectory_comparison"
        print("Getting contact trajectories for second trajectory")

        dists2 = getDistForTraj(traj2, sel1, sel2, resContacts, com, skip)
        os.makedirs(dirTraj2, exist_ok=True)

        saveSingleTraj(dirTraj2, dists2, header, resContacts)

        print("Comparing trajectories")

        os.makedirs(dirCmp, exist_ok=True)

        cmpTrajs(dirCmp, dists, dists2, resContacts)
    